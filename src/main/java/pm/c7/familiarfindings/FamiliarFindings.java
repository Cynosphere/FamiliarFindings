package pm.c7.familiarfindings;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class FamiliarFindings implements ModInitializer, ClientModInitializer {
    public static final String MOD_ID = "familiarfindings";

    public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.create(new Identifier(MOD_ID, "itemGroup"))
            .icon(() -> new ItemStack(Blocks.DARK_PRISMARINE))
            .build();

    @Override
    public void onInitialize() {
    }

    @Override
    public void onInitializeClient() {
    }
}
